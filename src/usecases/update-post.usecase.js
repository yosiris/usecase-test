import { PostsRepository } from "../repositories/posts.repository";

export class UpdatePost {
  static async execute(posts = [], postModel) {
    const repository = new PostsRepository();
    const updatePost = await repository.updatePost(postModel);

    const updatePostModel = {
      id: updatePost.id,
      title: updatePost.title,
      content: updatePost.body,
    };

    return posts.map((post) =>
      post.id === updatePostModel.id ? updatePostModel : post
    );
  }
}
