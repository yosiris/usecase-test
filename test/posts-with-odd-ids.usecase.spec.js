import { PostsRepository } from "../src/repositories/posts.repository";
import { PostsWithOddIdsUseCase } from "../src/usecases/posts-with-odd-ids.usecase";
import { POSTS } from "./fixtures/posts";

jest.mock("../src/repositories/posts.repository");

describe("Posts with odd identifiers use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should get all posts", async () => {
    PostsRepository.mockImplementation(() => {
      return {
        getAllPosts: () => {
          return POSTS;
        },
      };
    });

    const posts = await PostsWithOddIdsUseCase.execute();

    expect(posts.every((post) => post.id % 2 !== 0)).toBeTruthy();
  });
});
