import { PostsRepository } from "../repositories/posts.repository";

export class DeletePost {
  static async execute() {
    const repository = new PostsRepository();
    const deletePost = await repository.deletePost();
    return deletePost;
  }
}
