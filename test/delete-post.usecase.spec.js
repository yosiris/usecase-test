import { PostsRepository } from "../src/repositories/posts.repository";
import { DeletePost } from "../src/usecases/delete-post.usecase ";

jest.mock("../src/repositories/posts.repository");
describe("Delete post use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should delete post post", async () => {
    PostsRepository.mockImplementation(() => {
      return {
        deletePost: () => {
          return {};
        },
      };
    });

    const deletePost = await DeletePost.execute();
    expect(deletePost).toEqual({});
  });
});
